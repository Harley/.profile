# Harley Acheson
* [devtalk.blender.org](https://devtalk.blender.org/) [Harleya](https://devtalk.blender.org/u/Harleya/) | [Activity](https://devtalk.blender.org/u/harleya/activity)
* [blender.chat](https://blender.chat/channel/blender-coders) [@Harleya](https://blender.chat/direct/Harleya)
* [blenderartists.org](https://blenderartists.org/) [Harley](https://blenderartists.org/u/harley)
* Email: [harley.acheson@gmail.com](harley.acheson@gmail.com)
* Facebook: [facebook.com/harley.acheson](https://www.facebook.com/harley.acheson)
* Strava: [Cervélo S5, Cannondale SuperX, Specialized SX Trail](https://www.strava.com/athletes/23497312)
* Location: [Cowichan Valley](https://www.google.com/maps/@48.898801,-123.7466172,10z), [Vancouver Island](https://www.google.com/maps/@49.6222093,-124.8984774,7.75z), [British Columbia](https://www.google.com/maps/@55.0723918,-125.0111362,5.25z), [Canada](https://www.google.com/maps/@54.8577813,-96.0627005,4.75z)
* Time Zone: [Pacific Standard (UTC-8)](https://time.is/UTC-8)
* Platform: Windows

## Interests
User Interface, pixel-perfect layout, spelling, grammar, fonts, icons, images, input, internationalization, translation, accessibility.

## Weekly Reports
* [Weekly Reports 2025](https://projects.blender.org/Harley/.profile/src/branch/main/reports/2025.md)
* [Weekly Reports 2024](https://projects.blender.org/Harley/.profile/src/branch/main/reports/2024.md)
* [Weekly Reports 2023](https://projects.blender.org/Harley/.profile/src/branch/main/reports/2023.md)
